<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'nombre_rep_legal',
        'documento_rep_legal',
        'ruc',
        'razon_social',
        'nombre_comercial',
        'actividad_comercial',
        'email',
        'email2',
        'telefono',
        'telefono2',
        'direccion',
        'ciudad',
        'vendedor',
        'categoria_id',
    ];
}
