<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $fillable = [
        'tipo',
        'pvp',
    ];

    public function productos()
    {
        return $this->hasMany('App\Producto');
    }

    public function contrato_cliente()
    {
        return $this->hasMany('App\Contrato_Cliente');
    }
}
