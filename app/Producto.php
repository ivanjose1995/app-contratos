<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = [
        'nombre',
        'contrato_id',
    ];

    public function contrato()
    {
        return $this->belongsTo('App\Contrato');
    }
    
    public function contrato_cliente()
    {
        return $this->hasMany('App\Contrato_Cliente');
    }

    public function producto_cliente()
    {
        return $this->hasMany('App\Producto_Cliente');
    }
}
