<?php

namespace App\Http\Controllers;


use App\Cliente;
use App\Contrato;
use App\Producto;
use App\Contrato_Cliente;
use App\Producto_Contrato;

use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use NumeroALetras\NumeroALetras;

class ContratoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::all();
        $contratosClientes = Contrato_Cliente::paginate(8);

        return view('contratos.index',compact('contratosClientes','clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Cliente::all();
        $contratos = Contrato::all();

        return view('contratos.create',compact('clientes','contratos'));
    }

    public function buscar_contrato(Request $request){
        
        $id_cliente = $request->cliente;
        $codigo = $request->codigo;

        if($codigo == null)
        {
            if($id_cliente != "0" )
            {
                $contratosClientes = Contrato_Cliente::where('cliente_id',$id_cliente)->paginate(8);
            }
            else{
                $contratosClientes = Contrato_Cliente::paginate(8);  
            }
        }    
        else{
            $contratosClientes = Contrato_Cliente::where('codigo',$codigo)->paginate(8);
        }
        
        $clientes = Cliente::all();
        return view('contratos.index',compact('contratosClientes','clientes'));
        
    }

    public function seleccionar_tipo_contrato(Request $request){

        $cliente=$request->cliente;
        $tipo_contrato=$request->tipo_contrato;

        if($tipo_contrato == 1)
        {
            $productos = $this->mostrarProductosContrato($tipo_contrato);
            return view('contratos.createJavaPremium',compact('cliente','tipo_contrato','productos'));
        }
        elseif($tipo_contrato == 2)
        {
            return view('contratos.createServiciosContables',compact('cliente','tipo_contrato'));
        }
        elseif($tipo_contrato == 3)
        {
            return view('contratos.createOrganizadorElectronico',compact('cliente','tipo_contrato'));
        }
    }

    public function mostrarProductosContrato ($id){

        $productos = Producto::where('contrato_id',$id)->get();
		// if ($request->ajax()) {
		// 	return response()->json($productos, 200);
        // }
        return $productos;
    }

    public function store(Request $request)
    {
        $contratoCliente = new Contrato_Cliente();
        $contratoCliente->contrato_id = $request->tipo_contrato;
        $contratoCliente->cliente_id = $request->cliente;
        $contratoCliente->fecha = $request->fecha;
        $contratoCliente->codigo= $this->generar_codigo_contrato();
        if($request->tipo_contrato == 2){
            $contratoCliente->pvp = $request->pvp;
        }
        if($request->tipo_contrato == 3){
            $contratoCliente->pvp = $request->pvp;
            $contratoCliente->horas_capacitacion = $request->horas_capacitacion;
            $contratoCliente->cuentas_asignadas = $request->cuentas_asignadas;
        }
        $contratoCliente->estado = 'pendiente';
        $contratoCliente->save();

        // guardando listado d productos si el contrato es java premium
        if($request->tipo_contrato == 1){
            $this->guardar_productos_JavaPremium($request->productos,$contratoCliente->id);
        }

    //     //guardar archivo pdf del contrato javaPremium
    //     $productos = Producto_Contrato::where('contrato_cliente_id',$contratoCliente->id)->get();
    //     if($contratoCliente->contrato->tipo == "VERSION JAVA “PREMIUM"){

    //         $pdf = PDF::loadView('pdf-contratos.contrato_javaPremium', compact('contratoCliente','productos'))
    //                     ->save( storage_path('app/public/contratos/')."$contratoCliente->codigo".'.pdf');
    //    }

        return redirect("/contratos/mostrar/$contratoCliente->id");
    }

    public function generar_codigo_contrato()
    {
        $band=false;
        do{
            $codigo = rand(1000,9999);
            $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            for($x = 0; $x < 4; $x++){
                $aleatoria = substr(str_shuffle($caracteres), 0, 4);
            }
            $codigo = "$aleatoria"."$codigo";
            $find = Contrato_Cliente::where('codigo',$codigo)->get();
            if($find->isEmpty()){
                $band = true;
            }
        }
        while($band == false);

        return $codigo;
    }

    public function guardar_productos_JavaPremium($productos,$cliente)
    {
         // guardando productos asignados al contrato del cliente
         $stringProductos = $productos;
         $arrayProductos = explode(',',$stringProductos);

         foreach($arrayProductos as $producto ){
             $productoContrato = new Producto_Contrato();
             $productoContrato->producto_id = $producto;
             $productoContrato->contrato_cliente_id = $cliente;
             $productoContrato->save();
         }
    }

    public function show($id)
    {

        $contratoCliente = Contrato_Cliente::find($id);

        return view('contratos.show',compact('contratoCliente'));
    }

    public function generar_pdf($id){

       $contratoCliente = Contrato_Cliente::find($id);

       $cifraPVP = NumeroALetras::convertir($contratoCliente->pvp, 'dólares');
       $arrayFecha = explode('-',$contratoCliente->fecha);
       $meses = [ 'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre' ];
       $mes = $meses[$arrayFecha[1]-1];
       $año = $arrayFecha[0];
       $dia = $arrayFecha[2];
       $cifraDia = NumeroALetras::convertir($arrayFecha[2]);
       $cifraDiaRed = explode(' ', $cifraDia);
       $cifraDia = $cifraDiaRed[0]; 
       
       if($contratoCliente->contrato_id == 1){
            $productos = Producto_Contrato::where('contrato_cliente_id',$contratoCliente->id)->get();
            $pdf = PDF::loadView('pdf-contratos.contrato_javaPremium', compact('contratoCliente','productos','dia','mes' ,'año'));
       }
       elseif($contratoCliente->contrato_id == 2){
            $pdf = PDF::loadView('pdf-contratos.contrato_serviciosContables', compact('contratoCliente','cifraPVP','dia','mes' ,'año'));
       }
       elseif($contratoCliente->contrato_id == 3){
            $pdf = PDF::loadView('pdf-contratos.contrato_organizadorElectronico', compact('contratoCliente','cifraPVP','cifraDia','mes' ,'año'));
        }    

        return $pdf->stream('contrato.pdf');
    }

    public function enviar_email($id){

        $contratoCliente = Contrato_Cliente::find($id);

        if($contratoCliente->contrato_id == 1){
            $productos = Producto_Contrato::where('contrato_cliente_id',$contratoCliente->id)->get();
            $pdf = PDF::loadView('pdf-contratos.contrato_javaPremium', compact('contratoCliente','productos'));
       }
       elseif($contratoCliente->contrato_id == 2){
            $pdf = PDF::loadView('pdf-contratos.contrato_serviciosContables', compact('contratoCliente'));
       }
       elseif($contratoCliente->contrato_id == 3){
            $pdf = PDF::loadView('pdf-contratos.contrato_organizadorElectronico', compact('contratoCliente'));
        }

        $data = [];
        $emailDestino= $contratoCliente->cliente->email ;

        Mail::send('notificacion', $data , function ($mail)  use($pdf,$emailDestino) {
            $mail->from('admin@contratos.net', 'Administrador');
            $mail->to($emailDestino);
            $mail->subject(" contrato generado");
            $mail->attachData($pdf->output(), 'contrato.pdf');
        });

        $correoEnviado = true;
        return view('contratos.show',compact('contratoCliente','correoEnviado'));
    }


    public function edit($id)
    {
        $contratoCliente = Contrato_Cliente::find($id);
    
        if($contratoCliente->contrato->id == 1)
        {
            // si el contrato es java premium busca el listado de productos que puede elegir
            $productos = $this->mostrarProductosContrato($contratoCliente->contrato->id);
            return view('contratos.edit',compact('contratoCliente','productos'));
        }
        
        return view('contratos.edit',compact('contratoCliente'));
    }

    public function update(Request $request)
    {


        $contratoCliente = Contrato_Cliente::find($request->id_contrato);
        
        $contratoCliente->fecha = $request->fecha;
        
        if($request->tipo_contrato == 2){
            $contratoCliente->pvp = $request->pvp;
        }
        if($request->tipo_contrato == 3){
            $contratoCliente->pvp = $request->pvp;
            $contratoCliente->horas_capacitacion = $request->horas_capacitacion;
            $contratoCliente->cuentas_asignadas = $request->cuentas_asignadas;
        }
        $contratoCliente->save();

        // guardando listado d productos si el contrato es java premium
        if($request->tipo_contrato == 1){
            $prodyctosBorrados =  Producto_Contrato::where('contrato_cliente_id',$request->id_contrato)->delete();
            $this->guardar_productos_JavaPremium($request->productos,$contratoCliente->id);
        }   

        return redirect("/contratos/mostrar/$contratoCliente->id");
    }

    public function mostrar_form_eliminar($id){

        $id_contrato = $id;

        return view('contratos.eliminar-contrato',compact('id_contrato'));

    }
    public function destroy(Request $request)
    {
       $id_contrato = $request->id_contrato;
       $prodyctosBorrados =  Producto_Contrato::where('contrato_cliente_id',$id_contrato)->delete();
       Contrato_cliente::destroy($id_contrato);

        return redirect()->route('contratos');
    }

    public function mostrar_formulario_finalizar($id){

        $contratoCliente = Contrato_Cliente::find($id);

        return view('contratos.finalizar',compact('contratoCliente'));
    }

    public function guardar_contrato_finalizado(Request $request){

        $contratoCliente = Contrato_Cliente::find($request->id_contrato);
        $ruta =$request->file('contrato_firmado')->store('contratos-firmados','public');
        $contratoCliente->contrato_firmado = $ruta;
        $contratoCliente->estado = 'finalizado';
        $contratoCliente->save();

        $finalizado = true;
        return view('contratos.show',compact('contratoCliente','finalizado'));

    }

    public function descargar_contrato($id){
        
        $contratoCliente = Contrato_Cliente::find($id);
        $ruta = "storage/".$contratoCliente->contrato_firmado;
        
        return response()->download($ruta);

    }
}
