<?php

namespace App\Http\Controllers;
use App\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes= Cliente::paginate(8);

        return view('clientes.index',compact('clientes'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = DB::table('categoria_clientes')->get();
        return view('clientes.create',compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $cliente = new Cliente();
        
        $cliente->nombre_rep_legal = $request->nombre_rep_legal; 
        $cliente->documento_rep_legal = $request->documento_rep_legal;
        $cliente->ruc = $request->ruc;
        $cliente->razon_social = $request->razon_social;
        $cliente->nombre_comercial = $request->nombre_comercial;
        $cliente->actividad_comercial = $request->actividad_comercial;
        $cliente->email = $request->email;
        $cliente->email2 = $request->email2;
        $cliente->telefono = $request->telefono;
        $cliente->telefono2 = $request->telefono2;
        $cliente->ciudad = $request->ciudad;
        $cliente->direccion = $request->direccion;
        $cliente->vendedor = $request->vendedor;
        $cliente->categoria_cliente_id = $request->categoria;
        
        $cliente->save();

         return redirect('/clientes');
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        $categorias = DB::table('categoria_clientes')->get();
        return view('clientes.editar',compact('categorias','cliente'));
    }

  
    public function update(Request $request)
    {
        
        $cliente = Cliente::find($request->id_cliente);
        
        $cliente->nombre_rep_legal = $request->nombre_rep_legal; 
        $cliente->documento_rep_legal = $request->documento_rep_legal;
        $cliente->ruc = $request->ruc;
        $cliente->razon_social = $request->razon_social;
        $cliente->nombre_comercial = $request->nombre_comercial;
        $cliente->actividad_comercial = $request->actividad_comercial;
        $cliente->email = $request->email;
        $cliente->email2 = $request->email2;
        $cliente->telefono = $request->telefono;
        $cliente->telefono2 = $request->telefono2;
        $cliente->ciudad = $request->ciudad;
        $cliente->direccion = $request->direccion;
        $cliente->vendedor = $request->vendedor;
        $cliente->categoria_cliente_id = $request->categoria;
        
        $cliente->save();

         return redirect('/clientes');
    }

    public function mostrar_form_eliminar ($id){

        $cliente = cliente::find($id);

        return view('clientes.eliminar',compact('cliente'));

    }
    public function destroy(Request $request)
    {

        Cliente::destroy($request->id_cliente);

        return redirect()->route('clientes');
    }
}
