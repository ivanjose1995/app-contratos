<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato_Cliente extends Model
{
    protected $fillable = [
        'cliente_id',
        'contrato_id',
        'codigo',
        'fecha',
        'pvp',
        'horas_capacitacion',
        'cuentas_asignadas',
    ];

    protected $table = 'contratos_clientes';

    public function contrato()
    {
        return $this->belongsTo('App\Contrato');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }

    public function productos()
    {
        return $this->hasMany('App\Producto_Contrato');
    }
}
