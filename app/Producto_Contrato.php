<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto_Contrato extends Model
{
    protected $fillable = [
        'contrato_cliente_id',
        'producto_id'
    ];


    public function contratoCliente()
    {
        return $this->belongsTo('App\Contrato_Cliente');
    }

    
    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }
    

    protected $table = 'productos_contratos';
}
