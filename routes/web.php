<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('contratos')->group(function () {
    Route::get('/','ContratoController@index')->name('contratos');
    Route::get('/crear','ContratoController@create')->name('contratos.crear');
    Route::post('/tipo-contrato','ContratoController@seleccionar_tipo_contrato')->name('contratos.seleccionar-tipo-contrato');
    Route::get('/productos-contrato/{id}','ContratoController@mostrarProductosContrato');
    Route::post('/guardar','ContratoController@store')->name('contratos.guardar');
    Route::get('/mostrar/{id}','ContratoController@show')->name('contratos.mostrar');
    Route::get('/generar-pdf/{id}','ContratoController@generar_pdf')->name('contratos.pdf');
    Route::get('/enviar-email/{id}','ContratoController@enviar_email')->name('contratos.email');
    Route::post('/buscar-contrato','ContratoController@buscar_contrato')->name('contratos.buscar');
    Route::get('/mostrar-eliminar/{id}','ContratoController@mostrar_form_eliminar')->name('contratos.mostrar-eliminar');
    Route::post('/contrato-eliminar','ContratoController@destroy')->name('contratos.eliminar');
    Route::get('/editar/{id}','ContratoController@edit')->name('contratos.editar');
    Route::post('/contrato-actualizar','ContratoController@update')->name('contratos.actualizar');
    Route::get('/finalizar/{id}','ContratoController@mostrar_formulario_finalizar')->name('contratos.finalizar');
    Route::post('/guardar-contrato-finalizado','ContratoController@guardar_contrato_finalizado')->name('contratos.guardar-contrato-finalizado');
    Route::get('/descargar/{id}' , 'ContratoController@descargar_contrato');
    
});

Route::prefix('clientes')->group(function () {
    Route::get('/', 'ClienteController@index')->name('clientes');
    Route::get('/crear', 'ClienteController@create')->name('clientes.crear');
    Route::post('/guardar', 'ClienteController@store')->name('clientes.guardar');
    Route::get('/editar/{id}','ClienteController@edit')->name('clientes.editar');
    Route::post('/actualizar','ClienteController@update')->name('clientes.actualizar');
    Route::get('/confirmar-eliminar/{id}','ClienteController@mostrar_form_eliminar')->name('clientes.confirmar-eliminar');
    Route::post('/eliminar','ClienteController@destroy')->name('clientes.eliminar');
});

