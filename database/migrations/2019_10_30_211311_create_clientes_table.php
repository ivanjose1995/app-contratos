<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_rep_legal');
            $table->string('documento_rep_legal');
            $table->string('ruc');
            $table->string('razon_social');
            $table->string('nombre_comercial');
            $table->string('actividad_comercial');
            $table->string('email');
            $table->string('email2');
            $table->string('telefono');
            $table->string('telefono2');
            $table->longText('direccion');
            $table->string('ciudad');
            $table->string('vendedor');
            $table->unsignedBigInteger('categoria_cliente_id');
                $table->foreign('categoria_cliente_id')->references('id')->on('categoria_clientes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
