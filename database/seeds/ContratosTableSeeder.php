<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContratosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contratos')->insert([
            'tipo' => 'Contrato VERSION JAVA “PREMIUM',
        ]);
        DB::table('contratos')->insert([
            'tipo' => 'Contrato Servicios Contables',
        ]);
        DB::table('contratos')->insert([
            'tipo' => 'Contrato organizador electronico',
        ]);
    }
}
