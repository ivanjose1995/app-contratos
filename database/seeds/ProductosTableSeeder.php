<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //productos del contrato Java Premium
        
        DB::table('productos')->insert([
            'nombre' => 'Gestión Seguridad y auditoría',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'producGestión Contabilidad',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'Gestión Caja-Bancos',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'Gestión Ventas',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'Gestión Cuentas por Cobrar',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'Gestión Punto de venta',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'Gestión Compras',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'Gestión Cuentas por pagar',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'Gestión Inventarios',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'Gestión Producción',
            'contrato_id' => '1'
        ]);
        DB::table('productos')->insert([
            'nombre' => 'Facturaciópn Electrónica',
            'contrato_id' => '1'
        ]);



    }
}
