<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'nombre_rep_legal'=>'jose',
            'documento_rep_legal' =>'1234',
            'ruc' =>'112233',
            'razon_social' =>'acme',
            'nombre_comercial' =>'acme',
            'actividad_comercial' =>'comercio',
            'email' =>'hola',
            'email2' =>'hola2',
            'telefono' =>'57888',
            'telefono2' =>'6980',
            'direccion' =>'esquinq',
            'ciudad' =>'barcelona',
            'vendedor' =>'pedro',
            'categoria_cliente_id' =>'1',
        ]);
    }
}
