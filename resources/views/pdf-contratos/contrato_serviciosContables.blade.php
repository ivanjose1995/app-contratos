<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contrato</title>
    <!-- Styles -->
    {{-- <link href="{{ asset('css/contratoJavaPremium.css') }}" rel="stylesheet"> --}}
    <style>
        .tituloContrato{
            margin-left:40px;
            margin-right:40px;
            margin-top: 10px;
            font-size: 15px;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
        }
        .parrafo{
            text-align: justify;
            font-size: 13px;
            font-family: Arial, Helvetica, sans-serif;
        }
        .item-list{
            text-align: justify;
            margin-left: 25px;
        }
        .clienteFooter{
            margin-left: 50px;

        }
        .page-break {
            page-break-after: always;
        }
        main{
            margin-left:60px;
            margin-right:60px;
        }
        footer{
            margin-top: 120px;
            margin-left:50px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
        }
    </style>
</head>
<body>
    <header>
        <br>
        <div style="margin-right:140px;">
            <div style="float:right">
                    @php
                        echo DNS2D::getBarcodeHTML("$contratoCliente->codigo", "QRCODE",4,4);
                    @endphp
            </div>
            <h3 style="margin-left:340px;font-size:20px;font-family: Arial, Helvetica, sans-serif;">Código: {{ $contratoCliente->codigo }}</h3>
        </div>
        <br><br> 
        <h2 class="tituloContrato">CONTRATO DE PRESTACIÓN DE SERVICIOS PROFESIONALES</h2>
    </header>
    <br>
    <main>
        <p class="parrafo" >
           En la ciudad de {{ $contratoCliente->cliente->ciudad }}, a los {{$dia}} días del mes de {{$mes}} de {{$año}} se conviene en celebrar el presente contrato de PRESTACIÓN DE SERVICIOS PROFESIONALES entre:
           el señor <strong> {{ $contratoCliente->cliente->nombre_rep_legal }} </strong>  en con cedula de ciudadanía Nº <strong> {{ $contratoCliente->cliente->documento_rep_legal }} </strong>  en calidad de GERENTE GENERAL de la compañía <strong>{{$contratoCliente->cliente->razon_social}}</strong>  con RUC <strong>{{ $contratoCliente->cliente->ruc }}</strong> ,
           A quien en adelante se le denominará simplemente  “ LA CONTRATANTE “ y por otra parte el Ingeniero <strong>BEDON CASTILLO MARLON DANILO</strong> , portador de la cédula de ciudadanía No. <strong>1719070292</strong>  por sus propios derechos a quién en adelante se lo denomina simplemente “EL PROFESIONAL”.
           Los comparecientes son ecuatorianos, domiciliados la ciudad de Santo Domingo, y en goce de su capacidad civil para ejercer derechos y contraer obligaciones,
           quienes libre y voluntariamente convienen suscribir éste contrato de servicios profesionales contenido en las siguientes cláusulas
        </p>
        <p class="parrafo">
            PRIMERA.- ANTECEDENTES:
        </p>
        <p class="item-list parrafo">
            1.1 <strong>BEDON CASTILLO MARLON DANILO</strong> , es un profesional, que presta servicios completos de asesoría contable,
             dentro de los marcos de la ética, la moral, la justicia y el derecho,
             circunscrito las inquietudes y necesidades de quienes han solicitado mis servicios profesionales.
        </p>
        <p class="parrafo item-list">
            1.2. “LA CONTRATANTE” requiere contratar los servicios de un profesional en contabilidad, 
            para la contabilización de los registros y documentación de su actividad económica.
        </p>
        <p class="parrafo item-list">
            1.3. “EL PROFESIONAL” posee el título de tercer nivel en Ingeniería en Finanzas y Auditoria CPA, 
            además  justifica una amplia experiencia en procesos de procesamiento de registros contables.
        </p>
        <p class="parrafo ">
            TERCERA.- PRESTACIÓN DE SERVICIOS:
            <br><br>
            Con los antecedentes previamente expuestos, EL CONTRATANTE contrata los servicios profesionales de <strong>BEDON CASTILLO MARLON DANILO</strong> , 
            quien por su parte, se compromete a prestar sus servicios específicamente de procesamiento de registros contables y para elaborar las declaraciones de IVA, Retenciones Renta,
             Renta Anual, anexos RDEP, ATS, que se deben enviar al Servicio de Rentas Internas, para lo cual EL CONTRATANTE remitirá al profesional diariamente toda la documentación física y digital, que se genere en su actividad económica, 
            incluye también si fuera del caso el mantenimiento de reuniones máximo 2 por mes.
            <br>
            Actividades no descritas en el párrafo anterior serán facturadas según políticas del PROFESIONAL.
        </p>
        <p class="parrafo ">
            CUARTA.- HONORARIOS:
        </p>
        <p class="parrafo item-list ">
            4.1)    Por la asesoría contable que EL PROFESIONAL prestará a LA CONTRATANTE,  cuyas actividades se detallaron en la cláusula precedente,
             se compromete LA CONTRATANTE a cancelar a EL PROFESIONAL de forma mensual por concepto de honorarios profesionales la suma de $ {{$contratoCliente->pvp}} ({{$cifraPVP}}) más IVA.  Este pago lo realizará en moneda de curso legal, mediante efectivo, cheque o transferencia bancaria,
             previa a la entrega de la respectiva factura en tiempo máximo de los primeros 10 días de cada mes.
        </p>
        <div class="page-break"></div>
        <br><br>
        <p class="parrafo">
            QUINTA.-  HORARIO:
            <br><br>
            Dada la naturaleza del presente contrato, EL PROFESIONAL no está sujeto a un horario  fijo, ni lugar de trabajo, en razón de que no existe ningún vínculo o relación de dependencia entre las partes, así mismo ninguna obligación ni participación en los procesos operativos de LA CONTRATANTE. 
            No obstante, EL PROFESIONAL  atenderá con suma diligencia y de manera oportuna los requerimientos de LA CONTRATANTE
        </p>
        <p class="parrafo">
            SEXTA.- VIGENCIA DEL CONTRATO:
            <br>
            El presente contrato tendrá un plazo de duración de UN (1) año contados a partir de su suscripción, cualquiera de las partes podrá dar por terminado el contrato notificando 15 días antes del vencimiento del mismo, caso contrario se deberá realizar la renovación y actualización de cláusulas para el periodo siguiente.
            <br><br>
            El contrato se dará por terminado en caso de atrazo de pagos por más de 1 mes, salvo consentimiento por escrito de EL PROFESIONAL.
        </p>
        <br>
        <p class="parrafo">
            SEPTIMA.- RELACION DE DEPENDENCIA:
            <br><br>
            En vista de que el presente Contrato es de naturaleza civil, LA CONTRATANTE  no tendrá ninguna obligación laboral, ni patronal ni con EL PROFESIONAL, ni con el personal que éste contrate para la ejecución de este contrato civil, por lo que no están ni se entienden incorporadas al presente Contrato, las disposiciones del Código de Trabajo, ni las demás relativas a este tipo de relaciones.  Por tanto, las partes contratantes, no podrán invocar las disposiciones contenidas en las Leyes Laborales en cualquier disputa, interpretación o reclamo que tuvieren.
        </p>
        <p class="parrafo">
            OCTAVA.- DIVISIBILIDAD:
            <br><br>
            Si cualquier estipulación o conjunto de estas, contenidas en el presente Contrato, se consideraren, nulas o ineficaces, por cualquier motivo que fuera, éstas se entenderán por no escritas, no obstante, este hecho no afectará la validez de las demás disposiciones, emanadas del presente Contrato, peor aún se podrá alegar su derogación o ausencia de obligatoriedad.
        </p>
        <br>
        <p class="parrafo">
            NOVENA.- CONFIDENCIALIDAD:
            <br><br>
            <span>
                    Las partes acuerdan que toda la información que proporcionare LA CONTRATANTE  a EL PROFESIONAL  para la asesoría que éste último prestará, tendrá el carácter de CONFIDENCIAL, por lo que la misma no será susceptible de reproducciones,
                    copias magnéticas o fotostáticas, ni de ninguna otra forma de reproducción, a menos que sea necesario para el cumplimiento del presente Contrato, por lo que EL PROFESIONAL  se responsabiliza civil y penalmente por la divulgación,
                    reproducción, traspaso, dispersión o copia total o parcial de la información que LA CONTRATANTE  le proporcione. Adicionalmente, 
                    la información confidencial incluye, sin limitarse a ello, estrategias, objetivos, políticas, proyectos, prioridades, cronogramas de trabajo, 
                    sistemas tecnológicos, manuales, reglamentos, y en general todos los conceptos relacionados, utilizados o desarrollados por LA CONTRATANTE.
                    El PROFESIONAL será el único responsable de la divulgación no autorizada de la información obtenida durante la ejecución del presente Contrato o como consecuencia del mismo por parte de todos aquellos funcionarios, socios, colaboradores,
                    administradores, dependientes, empleados o compañías que tengan una relación con EL PROFESIONAL,
                    y responderá frente a LA CONTRATANTE   por dicha divulgación de información. 
                    Será además de exclusiva responsabilidad de EL PROFESIONAL adoptar las medidas o precauciones que crea pertinentes para evitar que las mencionadas personas divulguen la antes referida información.
                    No obstante, EL PROFESIONAL no será responsable de la divulgación de la información confidencial cuando ésta haya sido entregada por LA CONTRATANTE a EL PROFESIONAL,
                    para a su vez ser entregada a otras personas, compañías u órganos públicos,
                    en cuyo caso esta información perderá el carácter de confidencial.  Las prohibiciones, obligaciones y derechos aquí contenidos,
                    estarán vigentes hasta CINCO (5) años después de la finalización del presente Contrato.
           </span>    
        </p>
        <p class="parrafo">
            DÉCIMA.- JURISDICCIÓN Y COMPETENCIA:
            <br><br>
            Las partes se comprometen a ejecutar de buena fe las obligaciones recíprocas que contraen mediante este Contrato, y a realizar todos los esfuerzos requeridos para superar, de mutuo acuerdo, cualquier controversia.  Toda controversia o diferencia derivada de la aplicación, validez, interpretación, nulidad o cumplimiento del presente Contrato será resuelta con la asistencia de un mediador del Centro de Arbitraje y Mediación de la Cámara de Comercio de Santo Domingo.  En el evento que el conflicto no fuere resuelto mediante este procedimiento,
            las partes someten sus controversias a la resolución de un Tribunal de Arbitraje que se sujetará a lo dispuesto en la Ley de Arbitraje y Mediación, el Reglamento del Centro de Arbitraje y Mediación de la Cámara de Comercio de Santo Domingo y las siguientes normas:
        </p>
        <p class="parrafo item-list">
            12.1)             UN (1) árbitro será designado por LA CONTRATANTEUN (1) árbitro será elegido por EL PROFESIONAL;  y, el tercero por los dos previamente designados.
        </p>
        <p class="parrafo item-list">
            12.2)             Las partes renuncian a la jurisdicción ordinaria, para someterse y cumplir el laudo arbitral, renunciando a interponer recurso alguno con posterioridad.
            
        </p>
        <p class="parrafo item-list">
            12.3)             Para la ejecución de medidas cautelares el Tribunal Arbitral está facultado para solicitar el auxilio de los funcionarios públicos, judiciales y administrativos sin que sea necesario recurrir a un juez ordinario.
        </p>
        <p class="parrafo item-list">
            12.4)             El Tribunal estará integrado por TRES (3) árbitros; y,
        </p>
        <p class="parrafo item-list">
            12.5)             El lugar de arbitraje será en las instalaciones del Centro de Arbitraje y Mediación de la Cámara de Comercio de Santo Domingo
        </p>
        <p class="parrafo ">
            DÉCIMA PRIMERA.- ACEPTACIÓN: Las partes contratantes aceptan y ratifican todas y cada una de las cláusulas precedentes, por así convenir a los de sus representadas. En cuanto no se opongan a las estipulaciones presentes, 
            tendrán la prerrogativa de incorporar a este Contrato, las disposiciones legales que puedan ser aplicables y compatibles con el mismo y convienen además que, en cualquier tiempo podrán modificar, rectificar, interpretar,
             ampliar o restringir los términos o cláusulas de la presente Convención, mediante acuerdo escrito, celebrado entre los contratantes y que sean posteriores a la suscripción del mismo.
            <br><br>
            Para constancia de lo expuesto, las partes firman el presente contrato  por triplicado.
        </p>
    </main>
    <footer>
        <p>
            <span> <strong>LA CONTRATANTE</strong> </span>
            <span style="margin-left:175px"> <strong>EL PROFESIONAL   </strong> </span>
            <br>
            <span> <strong>{{ $contratoCliente->cliente->razon_social}}</strong> </span>
            <span style="margin-left:265px"> <strong>Cedula: 1719070292</strong> </span>
            <br>
            <span> <strong>Cédula {{ $contratoCliente->cliente->documento_rep_legal}}</strong>
            <br>
            <span> <strong>Representante Legal</strong>         
        </p>
    </footer>
</body>
</html>
