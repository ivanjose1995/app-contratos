<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contrato</title>
    <!-- Styles -->
    {{-- <link href="{{ asset('css/contratoJavaPremium.css') }}" rel="stylesheet"> --}}
    <style>
        .tituloContrato{
            font-size: 15px;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            margin-left:50px;
            margin-right:50px;
        }
        .parrafo{
            text-align: justify;
            font-size: 14px;
            font-family: Arial, Helvetica, sans-serif;
        }
        .item-list{
            text-align: justify;
            margin-left: 20px;
        }
        .footer{
            margin-left: 50px;
            font-size: 14px;
            font-family: Arial, Helvetica, sans-serif; 
        }
        .minuscula{
            text-transform: lowercase;
        }
        .page-break {
            page-break-after: always;
        }
        main{
            margin-left:50px;
            margin-right:50px;
        }
    </style>
</head>
<body>
    <header>
        <br>
        <div style="margin-right:150px;">
            <div style="float:right">
                @php
                    echo DNS2D::getBarcodeHTML("$contratoCliente->codigo", "QRCODE",4,4);
                @endphp
            </div>
            <h3 style="margin-left:340px;font-size:20px;font-family: Arial, Helvetica, sans-serif;">Código: {{ $contratoCliente->codigo }}</h3>
        </div>
        <br><br>
        <h2 class="tituloContrato">CONTRATO DE LICENCIA DE USO DEL  SOFTWARE “ORGANIZADOR ELECTRONICO” V2.0</h2>
    </header>
    <br>
    <main>
        <p class="parrafo">En la ciudad de {{ $contratoCliente->cliente->ciudad }},
            a los  <span class="minuscula">{{$cifraDia}}</span> días del mes  {{$mes}} del {{$año}}, se celebra el presente contrato privado de LICENCIA de uso de software,
            al tenor de las siguientes cláusulas:
        </p>
        <p class="parrafo">
            <strong>Primera: COMPARECIENTES.-</strong>Comparecen a la firma del presente contrato por una parte la Ing. <strong>BURGOS CASQUETE VERONICA CECILIA</strong> ,
            con cedula de ciudadanía 1721800439 en calidad de representante legal de <strong>BEBUC CIA. LTDA. </strong>  Con RUC 2397651239001,
            a quien en adelante podrá denominarse LA EMPRESA, y por otra parte <strong> {{ $contratoCliente->cliente->nombre_rep_legal }} </strong> ,
            con cedula de ciudadanía Nº {{ $contratoCliente->cliente->documento_rep_legal }} quien en adelante y para efecto de este contrato se  denomina <strong>EL CLIENTE</strong> 
        </p>
        <p class="parrafo">
            <strong>Segunda: ANTECEDENTES:</strong>      
        </p>
        <p class=" item-list parrafo">
             b)  LA EMPRESA desde nace hace dos años aportando y desarrollado por medio de su departamento técnico el SISTEMA ADMINISTRATIVO “ORGANIZADOR ELECTRONICO”,
              el cual ha  sido objeto de constantes actualizaciones y mejoras para beneficio de sus usuarios. 
        </p>
        <p class=" item-list parrafo">
            c)  EL CLIENTE desea instalar el SISTEMA ADMINISTRATIVO “ORGANIZADOR ELECTRONICO” en su empresa o negocio, datos del cliente: 
        </p>
        <p class=" item-list parrafo">
                CIUDAD: {{ $contratoCliente->cliente->ciudad }} 
                <br>
                RAZON SOCIAL: {{ $contratoCliente->cliente->razon_social }}  
                <br>
                RUC: {{ $contratoCliente->cliente->ruc }} 
                <br>
                TELEFONOS: {{ $contratoCliente->cliente->telefono }}  / {{ $contratoCliente->cliente->telefono2 }} 
                <br> 
                CORREO1: {{ $contratoCliente->cliente->email }} 
                <br>
                DIRECCION: {{ $contratoCliente->cliente->direccion }} 
                <br>
                CUENTAS ASIGNADAS: {{ $contratoCliente->cuentas_asignadas }} 
                <br>
        </p>
        <p class=" item-list parrafo">
            d)  En adelante SISTEMA ADMINISTRATIVO ORGANIZADOR ELECTRONICO versión 2.0 se denominara simplemente como “ORGANIZADOR ELECTRONICO”.  
        </p>
        <p class="parrafo"> 
            <strong>Tercera:  OBJETO  DEL  CONTRATO.-</strong>  LA  EMPRESA  por  medio  del  presente  contrato  se compromete con el CLIENTE a lo siguiente:     
        </p>
        <p class="parrafo">
            Instalar  en  la  oficina matriz  del  CLIENTE en un equipo servidor y  demás equipos en red al momento de la instalación.  
        </p>
        <p class="parrafo">
                El Software Administrativo “ORGANIZADOR ELECTRONICO”, mismo que tiene las siguientes características: 
        </p>
        <ul>
            <li class="parrafo">     
                 Permite convertir sus documentos electrónicos en formato PDF estándar según la institución de control.     
            </li>
            <br>
            <li class="parrafo">   
                Visualizar todos sus facturas electrónicas en una hoja de cálculo para administración y análisis de su información                   
            </li>
            <br>
            <li class="parrafo">
                Asignación definida de nombre detallado en cada factura electrónica en formato PDF y XML.
            </li>
            <br>
            <li class="parrafo">
                Descarga automática de comprobantes de venta recibidos de forma electrónica desde el portal del SRI mientras el web service de la institución lo permita y este accesible.
            </li>
        </ul>
        <div class="page-break">
        </div>
        <br><br>
        <p class="parrafo">
            <strong>Cuarta:</strong> USO DEL SISTEMA.- El CLIENTE se compromete a utilizar el sistema para fines legales y asume toda la responsabilidad del tipo de información que utilice además de respetar los RUC autorizados a LA EMPRESA para su uso
        </p>
        <p class="parrafo">
            <strong>Quinta:</strong>PRECIO.-  EL  CLIENTE.-se  compromete  a  pagar  el valor de  $ {{$contratoCliente->pvp}} ({{$cifraPVP}}) MAS IVA por la adquisición del sistema a la instalación y a cancelar anualmente desde el según año el 16.65% del SBU vigente al vencimiento,
             por concepto de mantenimiento del sistema y actualización de versiones,
             versión estándar de la siguiente manera: 100% a la instalación.
        </p>
        <p class="parrafo">
            LA EMPRESA se compromete a cumplir con la capacitación de {{$contratoCliente->horas_capacitacion}} horas, entregar video de uso para guía del usuario
        </p>
        <p class="parrafo">
            COSTOS ADICIONALES.- Se incurre en valores adicionales en los siguientes casos: 
        </p>
        <ul>
            <li class="parrafo">   
                Realizar un reporte personalizado, primero se hace el análisis según la complejidad del reporte y se cotiza.   
            </li>
            <br>
            <li class="parrafo">
                En caso de formateo de sus equipos o agregar equipos adicionales a la red de trabajo, el costo de generar nuevamente la licencia es de $15.00 más la visita técnica. 
            </li >
            <br>
            <li class="parrafo">
                En caso de actualización del sistema  por mejoras o cuando el SRI o alguna institución de control disponga cambios en los formatos actuales.
            </li>
        </ul>
        <p class="parrafo">
            <strong>Sexta:</strong>Sexta: LICENCIA DE USO.-  La EMPRESA se compromete a entregar a EL CLIENTE la licencia de uso del sistema “ORGANIZADOR ELECTRONICO”, cuyas cláusulas forman parte integral de este contrato,
             el convenio de licenciamiento es electrónico mismo que es aceptado en su totalidad al momento de la instalación en el servidor de datos ( 1 equipo ). 
        </p>
        <p class="parrafo">
                La EMPRESA entrega al CLIENTE el sistema estándar y se reserva el derecho de realizar, o no cambios. 
        </p>
        <p class="parrafo">
            <strong>Séptima:</strong>ASISTENCIA TÉCNICA. - La EMPRESA acepta y declara que el soporte técnico lo brindará la empresa BEBUC CIA. LTDA., misma que se compromete a la asistencia en tareas de capacitación y soporte,
             mediante su centros autorizados de soporte técnico, mismos que se encuentran publicados en nuestra página Web y que también son proporcionados a través del correo electrónico a solicitud de EL CLIENTE,
             con un tiempo de respuesta de máximo 24 horas dentro del perímetro urbano de Santo Domingo y 48 horas fuera del perímetro urbano, el costo de la hora técnica se facturará de acuerdo a las tablas vigentes en BEBUC CIA. LTDA.,
             en las fechas en las que sean solicitadas a la firma de este contrato la hora de soporte técnico tiene un costo de 30 USD (treinta dólares con 00/100 dólares de los Estados Unidos de América) más impuestos de Ley donde la primera hora no es fraccionable,
             fuera del perímetro urbano de Santo Domingo las tres primeras horas no son fraccionables. 
        </p>
        <p class="parrafo">
            <strong>Octava: </strong> ACTUALIZACIONES.-  La  EMPRESA  se  compromete  a  publicitar  y  actualizar el sistema  los cambios futuros que se den por disposiciones legales,
             EL CLIENTE optara por las actualización según la naturaleza y obligaciones de su empresa lo cual genera un costo referente al tipo de actualización.
        </p>
        <p class="parrafo">
            <strong>Novena: </strong> PERDIDA DE DERECHOS SOBRE ASISTENCIA TÉCNICA Y GARANTIA.- Toda la asistencia  técnica  que  EL  CLIENTE  requiera,  al  igual  que  las  actualizaciones  del  software entregado, deberá ser solicitada, a través de BEBUC CIA. LTDA. o su red de centros,
             ubicados dentro del territorio nacional, mismos que estarán siempre actualizados en la página Web de LA EMPRESA o confirmados mediante correo electrónico de LA EMPRESA. Nunca y bajo ningún concepto,
             el personal de LA EMPRESA o de los centros, podrá ser contratado ni directa ni indirectamente por el CLIENTE. 
        </p>
        <div class="page-break">
        </div>
        <br><br>
        <p class="parrafo">
            Si esta situación se presentara, esto es que el CLIENTE, por sí o por interpuesta persona contratare los servicios del personal técnico de LA EMPRESA o de los centros de forma ocasional o permanente, este perderá automáticamente la garantía sobre el producto adquirido,
            perderá el derecho a contar con la asistencia proporcionada por LA EMPRESA o los centros autorizados como son las actualizaciones de nuevas versiones del sistema.
        </p>
        <p class="parrafo">
            <strong>Décima: </strong>DOMICILIO Y JURISDICCIÓN.- Las partes ratifican y aprueban todas las estipulaciones  del presente contrato, señalan como domicilio la ciudad de Santo domingo y se someten al arbitraje de la Cámara de Comercio de Santo domingo, en caso de controversia las partes tratarán de solucionar sus diferencias de una forma directa y amistosa.
             En caso de no lograr ningún consenso, estas renuncian fuero y domicilio y de mutuo y voluntario acuerdo deciden acudir al centro de arbitraje de la Cámara de Comercio de Santo domingo en este caso nombrarán un árbitro de la lista proporcionada por este centro, y el árbitro designado emitirá su laudo arbitral en Derecho, 
             siguiendo el trámite y procedimiento  establecido  en  la  Ley  de  Mediación  y  Arbitraje,  el  cual  será  de  obligatorio cumplimiento para las partes. 
        </p>
        <p class="parrafo">
            Además declaran estar dispuestos a reconocer sus firmas y rúbricas incluidas en este documento ante uno de los jueces de lo Civil, si es necesario para la perfecta validez de este instrumento. 
        </p>
        <p class="parrafo">
                Para constancia las partes firman a continuación por triplicado
        </p> 

    </main>
    <br><br><br><br><br><br><br><br>
    <footer>
        <p class="footer">
            <span> <strong>Ing. Burgos Casquete Cecilia</strong> </span>
            <span style="margin-left:170px"> <strong>Sr {{ $contratoCliente->cliente->nombre_rep_legal}}</strong> </span>
            <br>
            <span> <strong>CC: 1721800439 </strong> </span>
            <span style="margin-left:250px"> <strong>CC {{ $contratoCliente->cliente->documento_rep_legal}}</strong> </span>
            <br>
            <span> <strong>GERENTE GENERAL</strong>
            </span><span style="margin-left:225px"> <strong>CONTADOR INDEPENDIENTE</strong> </span>
            <br>
            <span> <strong>BEBUC CIA. LTDA</strong>
            <span style="margin-left:235px;" > <strong> " EL CLIENTE "</strong> </span>
            <br>
            <span> <strong>“LA EMPRESA“</strong>
        </p>
    </footer>

</body>
</html>
