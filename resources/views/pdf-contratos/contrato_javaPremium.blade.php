<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contrato</title>
    <!-- Styles -->
    {{-- <link href="{{ asset('css/contratoJavaPremium.css') }}" rel="stylesheet"> --}}
    <style>
        .tituloContrato{
            margin-left:40px;
            margin-right:40px;
            font-size: 15px;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
        }
        .parrafo{
            text-align: justify;
            font-size: 12px;
            font-family: Arial, Helvetica, sans-serif;
        }
        .item-list{
            text-align: justify;
            margin-left: 20px;
        }
        .clienteFooter{
            margin-left: 50px;

        }
        .page-break {
            page-break-after: always;
        }
        main{
            margin-left:40px;
            margin-right:40px;
        }
        footer{
            margin-left:40px;
        }
    </style>   
</head>
<body>
    <header>
        <div style="margin-right:140px;">
            <div style="float:right">
                @php
                    echo DNS2D::getBarcodeHTML("$contratoCliente->codigo", "QRCODE",4,4);       
                @endphp
            </div>
            <h3 style="margin-left:350px;font-size:20px;font-family: Arial, Helvetica, sans-serif;">Código: {{ $contratoCliente->codigo }}</h3> 
        </div>
        <br><br>
        <h2 class="tituloContrato">CONTRATO DE CONCESIÓN DE USO DEL SOFTWARE ADMINISTRATIVO CONTABLE SACIORC {{$contratoCliente->contrato->tipo}}</h2>
    </header>
    <main>
        <p>En la ciudad de {{ $contratoCliente->cliente->ciudad }},
           al {{$dia}} de  {{$mes}} del {{$año}}, se celebra el presente contrato privado de concesión de uso de software,
           al tenor de las siguientes cláusulas:
        </p>
        <p class="parrafo" >
            <strong>Primera: COMPARECIENTES. </strong>  - Comparecen a la firma del presente contrato por una parte la señora ING. MIRIAN ANTONIETA JACHO JAMI,
            persona natural con RUC 1721800439001 a quien en adelante podrá denominarse EL PROVEEDOR, y por otra parte como persona juridica la empresa {{ $contratoCliente->cliente->razon_social }}, RUC {{ $contratoCliente->cliente->ruc }},
            representada legalmente por el Señor {{ $contratoCliente->cliente->nombre_rep_leagl }} con {{ $contratoCliente->cliente->documento_rep_leagl }}
            a quien en adelante y para efecto de este contrato se denomina EL PROPIETARIO.
        </p>
        <p class="parrafo"> <strong> Segunda: ANTECEDENTES:</strong> </p>
        <p class="item-list parrafo">
            a)	EL PROVEEDOR, es una persona natural, de derecho privado cuyo objeto social le permite realizar el desarrollo de todo tipo de software y de manera especial el software contable.
        </p>
        <p class="item-list parrafo">
            b)	EL PROVEEDOR desde hace más de 14 años ha concebido y desarrollado por medio de su departamento técnico el SISTEMA ADMINISTRATIVO CONTABLE INTEGRADO,
            el cual ha sido objeto de constantes actualizaciones y mejoras, y en la actualidad ha sido montado. 
        </p>
        <p class="item-list parrafo">
            c)	EL PROPIETARIO desea instalar el sistema administrativo Contable Integrado en su empresa o negocio.
        </p>
        <p class="item-list parrafo">
            d)	En adelante SaciOrc versión Java se denominará simplemente como SaciERP
        </p>
        <p class=" parrafo">
            <strong>Tercera: OBJETO DEL CONTRATO.</strong> -EL PROVEEDOR por medio del presente contrato se compromete con EL PROPIETARIO a lo siguiente:
        </p>
        <p class=" parrafo">
            Instalar en la oficina de EL PROPIETARIO, ubicado en la Ciudad de {{ $contratoCliente->cliente->ciudad }},{{ $contratoCliente->cliente->direccion }},
            el Software Administrativo Contable SaciERP, mismo que tiene las siguientes características: multiusuario, multibodega,
            multiempresa, integra por completo todos los módulos,
            y además es completamente aplicable a todo tipo de comercialización de artículos. 
        </p>
        <p class="parrafo">
            <strong> SaciERP “Licencia PerpetuaPremium”</strong>  incluye:
        </p>
        <ul>
            @foreach ($productos as $producto)
                <li>{{ $producto->producto->nombre}}</li>
            @endforeach
        </ul>
        <br>
        <p class="parrafo">
            <strong>Cuarta: LICENCIA DE USO PERPETUA DE SACIERP. </strong>  -EL PROPIETARIOno ha cancelado valor alguno por la Licencia de uso,
            ya que se trata de una Licencia promocional emitida antes del año 2017 a la fecha se emite a favor del Cliente la licencia de uso perpetua de SaciERP PREMIUM.
        </p>
        <div class="page-break"></div>
        <br>
        <p class="parrafo">
            <strong>Quinta: MANTENIMIENTO DE LICENCIAS SaciERP. </strong> -EL PROPIETARIO se compromete al realizar un pago anual correspondiente al 25.90% de un salario mínimo vital, 
            mismo que a la fecha de este contrato representa 100.00 usd (cien dólares americanos),
             más impuestos de ley, por concepto de mantenimiento de Licencias con; actualizaciones, módulos adicionales, funcionalidades nuevas y mejoradas que EL PROVEEDOR desarrolle sobreSaciERP.
            Este valor se ajustará anualmente de acuerdo con el incremento que sufra el salario mínimo vital.
        </p>
        
        <p class="parrafo">
            <strong>Sexta: SERVICIO DE ACTUALIZACIONES.</strong> -El PROPIETARIO se compromete a mantener siempre actualizado SaciERP con la última versión proporcionada por parte de EL PROVEEDOR,
            todas las actualizaciones que EL PROVEEDOR publique estarán siempre disponibles; para los PROPIETARIOS que estén al día en su aporte de “Mantenimiento de Licencias SaciERP” establecido en la cláusula Quinta de este contrato,
            la misma no tendrá costo alguno.  Caso contrario esto es, que no esté al día en su aporte de “Mantenimiento de Licencias SaciERP”, el costo de esta será del 40% del valor de la Licencia SaciERPGOLD, a la fecha de la actualización.
            En cualquiera de los dos casos no está incluido en este precio, las horas de asistencia técnica requeridas para ejecutar dicha actualización,
            estas horas se facturarán de acuerdo con lo establecido en la cláusula Séptima de este contrato, 
        </p>
        <p class="parrafo">
            <strong>Séptima: SOPORTE TECNICO.</strong> - Todo el soporte técnico que EL PROPIETARIO requiera para tareas de capacitación y/o inducción de SaciERP así como Soporte técnico sea este presencial o remoto se factura por hora de asistencia técnica de acuerdo a las tablas vigentes en EL PROVEEDOR,
            a la firma de este contrato la misma tiene un costo de 50.00 (cincuenta dólares americanos) más impuestos de ley, este valor puede cambiar en el futuro de acuerdo a las políticas de comercialización de EL PROVEEDOR.
            El tiempo de respuesta por parte de EL PROVEEDOR en este servicio es de una hora en modalidad no presencial y 24 horas para modalidad presencial dentro del perímetro urbano del DM de Quito fuera de este el tiempo de repuesta será de 48 horas.
        </p>
        <p class="parrafo">
            <strong>Octava: ACTIVACION DE SaciERP.</strong> -El servicio de activación de Licencia de SaciERP tanto para equipos nuevos o versiones actualizadas de SaciERP, tiene un costo de  50.00 usd (cincuenta dólares americanos) más impuestos de ley,
            este valor puede cambiar en el futuro de acuerdo a las políticas de comercialización de EL PROVEEDOR, más el servicio de la hora de soporte técnico, si este es solicitado.
             Nunca en ninguna circunstancia se activarán equipos nuevos o estaciones de trabajo que superen el número permitido en esta licencia estipulados en la Cláusula Tercera. 
        </p>
        <p class="parrafo">
           <strong>Novena: LICENCIA DE USO y GARANTIA TECNICA.</strong>   - En virtud de este Contrato EL PROVEEDOR se compromete a extender una Garantía Técnica sobre el programa de ordenador SaciERP bajo las siguientes limitaciones y condiciones:
        </p>
        <p class="item-list parrafo">
            a)	No se garantiza que el programa de ordenador cumpla con las expectativas del PROPIETARIO por fuera del desempeño usual técnico del programa. EL PROVEEDOR reconoce una Garantía Técnica de 365 días (12 meses) a partir de la firma del presente contrato,
             sobre defectos de fabricación (errores técnicos de programación exclusivamente) y la reparación sin costo alguno para EL PROPIETARIO. Esta garantía no incluye daños causados por la manipulación indebida del programa de ordenador ajena al personal técnico autorizado de EL PROVEEDOR, o generados por problemas en hardware,
            software, virus o factores externos que influyan el correcto funcionamiento del programa de ordenador SaciERP.
        </p>
        <p class="item-list parrafo">
            b)	El programa de ordenador SaciERP se entregará tal como fue desarrollado a esta fecha y versión, no se aceptarán reclamos posteriores sobre supuestas especificaciones no informadas por escrito y con anterioridad a la adquisición por parte de EL PROPIETARIO. 
        </p>
        <p class="item-list parrafo">
            c)	EL PROPIETARIO conoce que EL PROVEEDOR puede realizar modificaciones o actualizaciones al programa de ordenador sin necesidad de notificación alguna a ninguna persona.
        </p>
        <p class="item-list parrafo">
            d)	SaciERP queda activado y listo para ser utilizado al 100%, con cualquier tipo de Licenciamiento de la base de datos Oracle, pudiendo ser OracleXE, Estándar ONE, Estándar Enterprise u otras que pudiese adquirir en el mercado bajo esta marca, sin más limitaciones propias de Oracle.
        </p>
        <p class="item-list parrafo">
            e)	EL PROVEEDOR garantiza la conexión de usuarios ilimitados locales y remotos, dependiendo exclusivamente del tipo de licenciamiento adquirido estipulado en la cláusula TERCERA de este contrato.
        </p>
        <p class="item-list parrafo">  
            f)	EL PROVEEDOR entrega al CLIENTE el sistema estándar, Todo cambio requerido por escrito que se solicite dentro de la programación del programa de ordenador SaciERP, correrá a cargo de EL PROPIETARIO bajo las tarifas que EL PROVEEDOR establezca en caso de ser aceptado, factible y con tiempo estimado.
        </p>
        <p class="parrafo">
           <strong>Décima: LIMITE DE RESPONSABILIDAD. </strong>  - EL PROVEEDOR no se responsabiliza de los daños que pudiera provocar el mal uso del sistema a terceros, EL PROVEEDOR, no se responsabiliza por la presencia y/o funcionamiento de otros programas que no sean de EL PROVEEDOR,
            ni de otro software que este instalado en los equipos en donde funcione el software motivo de este contrato, ni por la pérdida de información producto de operaciones o administración negligente, ni es responsabilidad de EL PROVEEDOR la realización de back-up, respaldos, custodia o salvaguarda de la información de EL PROPIETARIO.
        </p>
        <div class="page-break"></div>
        <p class="parrafo">
            <strong> Décima Primera: PERDIDA DE DERECHOS SOBRE ASISTENCIA TÉCNICA Y GARANTIA.</strong>  - Toda la asistencia técnica que EL PROPIETARIO requiera, al igual que las actualizaciones del software entregado, deberá ser solicitada, a través de EL PROVEEDOR o su red de CAST, (Centro autorizado de servicio técnico),
             ubicados dentro del territorio nacional, mismos que estarán siempre actualizados en la página Web de EL PROVEEDOR. Nunca y bajo ningún concepto, el personal de EL PROVEEDOR o de los CAST, podrá ser contratado ni directa, ni indirectamente por parte de EL PROPIETARIO.
            <br><br>
            EL PROPIETARIO, no podrá contratar de ninguna manera, ni forma los servicios del personal de EL PROVEEDOR, incluso de ex empleados de EL PROVEEDOR hasta 24 meses posteriores a cualquier relación contractual que mantenga EL PROPIETARIO con EL PROVEEDOR.
            <br><br>
            Si esta situación se presentara, esto es que EL PROPIETARIO, por sí o por interpuesta persona contratare los servicios del personal técnico de EL PROVEEDOR o de los CAST de forma ocasional o permanente, este perderá automáticamente la garantía sobre el producto concesionado, 
            perderá el derecho a contar con la asistencia proporcionada por EL PROVEEDOR o los CAST, como son las actualizaciones de nuevas versiones del sistema, así también este contrato se dará por terminado de manera tácita.         
        </p>
        <p class="parrafo">
            <strong> DécimaSegunda: DOMICILIO Y JURISDICCIÓN.</strong>  - Las partes ratifican y aprueban todas las estipulaciones del presente contrato, señalan como domicilio la ciudad de Quito y se someten al arbitraje de la Cámara de Comercio de Quito,
            en caso de controversia las partes tratarán de solucionar sus diferencias de una forma directa y amistosa. En caso de no lograr ningún consenso, estas renuncian fuero y domicilio y de mutuo y voluntario acuerdo deciden acudir al centro de arbitraje de la Cámara de Comercio de Quito en este caso nombrarán un árbitro de la lista proporcionada por este centro,
            y el árbitro designado emitirá su laudo arbitral en Derecho, siguiendo el trámite y procedimiento establecido en la Ley de Mediación y Arbitraje, el cual será de obligatorio cumplimiento para las partes.
        </p>
        <p>
            Además, declaran estar dispuestos a reconocer sus firmas y rúbricas incluidas en este documento ante uno de los jueces de lo Civil, si es necesario para la perfecta validez de este instrumento.
        </p>
        <p>
            Para constancia las partes firman a continuación por triplicado,
        </p>
    </main>
    <br><br><br><br><br><br><br><br>
    <footer>
        <p>
            <span> <strong>Sra. Ing. Mirian Jacho Jami</strong> </span> 
            <span style="margin-left:175px"> <strong>{{ $contratoCliente->cliente->nombre_rep_legal}}</strong> </span>
            <br>
            <span> <strong>RUC 1712117975001 </strong> </span> 
            <span style="margin-left:215px"> <strong> Gerente general</strong> </span>
            <br>
            <span> <strong>SaciERP Comunidad de Usuarios</strong> 
            </span><span style="margin-left:140px"> <strong>{{ $contratoCliente->cliente->razon_social}}</strong> </span>
            <br>
            <span style="margin-left:375px;" > <strong> Cédula {{ $contratoCliente->cliente->documento_rep_legal}}</strong> </span>
        </p>
    </footer>
    
</body>
</html>