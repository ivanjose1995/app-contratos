@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center m-4">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header justify-content-center">
                    <h3 class="text-center">Detalles del contrato:</h3>
                </div>

                <div class="card-body">
                    <ul>
                        <li class="item-list-contrato">Cliente: {{ $contratoCliente->cliente->nombre_rep_legal }}</li>
                        <li class="item-list-contrato">Cédula: {{ $contratoCliente->cliente->documento_rep_legal }}</li>
                        <li class="item-list-contrato"> Empresa: {{ $contratoCliente->cliente->razon_social }}</li>
                        <li class="item-list-contrato">RUC: {{ $contratoCliente->cliente->ruc }}</li>
                        <li class="item-list-contrato"> Tipo de Contrato: {{ $contratoCliente->contrato->tipo }}</li>
                        <li class="item-list-contrato"> Fecha: {{ $contratoCliente->fecha }}</li>
                        <li class="item-list-contrato"> Estado: {{ $contratoCliente->estado }}</li>
                        <li class="item-list-contrato" >Código: {{ $contratoCliente->codigo }}</li>
                    </ul>
                    <div class="m-4">
                            @php
                            echo DNS2D::getBarcodeHTML("$contratoCliente->codigo", "QRCODE",4,4);      
                            @endphp
                    </div>
                   
                    <a href="{{ route ('contratos')}}" class="btn btn-large btn-dark"> Ir al listado de contratos</a>
                    <a href="/contratos/generar-pdf/{{$contratoCliente->id}}" class="btn btn-large btn-danger ml-2">
                         Ver pdf
                    </a>
                    <a href="/contratos/enviar-email/{{$contratoCliente->id}}" class="btn btn-large btn-primary ml-2">
                        Enviar contrato por Email
                   </a>
                   @if ( $contratoCliente->estado == 'pendiente')
                        <a href="/contratos/finalizar/{{$contratoCliente->id}}" class="btn btn-large btn-secondary ml-2">Finalizar contrato</a>    
                   @endif
                   @if ( $contratoCliente->estado == 'finalizado')
                        <a href="/contratos/descargar/{{$contratoCliente->id}}" class="btn btn-large btn-secondary ml-2">descargar contrato finalizado</a>    
                   @endif
                </div>

                @isset($correoEnviado)
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong>Correo Enviado exitosamente!</strong> Revisa tu bandeja de mensajes y encontraras el archivo adjunto del contrato.
                    </div>
                @endisset
                @isset($finalizado)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <strong>Archivo guardado exitosamente!</strong> se ha finalizado el contrato.
                </div>
                @endisset
                    
              
            </div>
        </div>
       <br><br>
        
    </div>
</div>
@endsection