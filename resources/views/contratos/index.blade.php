@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ route('contratos.buscar') }}" method="POST">   
            @csrf 
            <div class="form-row my-4">
                <div class="col-4">
                    <select class="form-control" name="cliente">
                        <option value="0"  selected>Seleccione un cliente</option>
                        @foreach ($clientes as $cliente)
                            <option value="{{ $cliente->id}}">{{$cliente->nombre_rep_legal}} , {{ $cliente->razon_social}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3">
                    <input type="text" class="form-control" name="codigo" placeholder="Ingrese el Código a buscar">
                </div> 
                <div class="col-4">
                    <button type="submit" class="btn btn-primary px-4">Buscar</button>
                </div>         
            </div>
        </form>
        <br>
        <div class="row mb-5 mt-4">
            <div class="col-4 offset-4">
                <h2>Listado de Contratos</h2>
            </div>
            <div class="col-2 offset-2">
                <a  class="btn btn-dark btn-large px-4 py-2" href="{{ route('contratos.crear') }}">Crear Contrato</a>
            </div>
        </div>
        <div class="row">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">Cliente</th>
                        <th scope="col">Tipo Contrato</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($contratosClientes as $contrato)
                        <tr>
                            <td>{{ $contrato->cliente->nombre_rep_legal }}</td>
                            <td>{{ $contrato->contrato->tipo }}</td>
                            <td>{{ $contrato->fecha }}</td>
                            <td>
                                <a class="btn btn-primary" href="/contratos/mostrar/{{$contrato->id}}" >Detalles</a>
                                <a class="btn btn-secondary ml-4" href="/contratos/editar/{{$contrato->id}}" >Editar</a>
                                <a class="btn btn-danger ml-4" href="/contratos/mostrar-eliminar/{{$contrato->id}}" >Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
               
            </table>
            {{ $contratosClientes->links() }}
        </div>
    </div>
    
@endsection