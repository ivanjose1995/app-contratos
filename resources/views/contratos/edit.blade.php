@extends('layouts.app')

@section('content')

<div class="container ">
        <div class="row justify-content-center m-4">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header justify-content-center">
                                <h3 class="text-center">Actualizar contrato</h3>
                        </div>
                        <div class="card-body">
                            <form  method="POST" action="{{ route('contratos.actualizar') }}" id="formContrato">
                                    @csrf
        
                                    <div class="col-6 offset-3 form-group">
                                        <label for="">Fecha:</label>
                                    <input class="form-control" type="date" name="fecha" id="fecha" required value="{{ $contratoCliente->fecha}}">
                                    </div>
                                    
                                    @if ($contratoCliente->contrato->id == 1)
                                        <div class="form-group col-6 offset-3" id="listaProductos">
                                            <h5 class="text-center mt-4 mb-4">Seleccione del listado de productos:</h5>
            
                                            @foreach ($productos as $producto)
                                                <div class="form-check">
                                                    <input class="form-check-input item-producto" type="checkbox" value="{{ $producto->id }}"   id=" {{ $producto->nombre }} ">
                                                    <label class="form-check-label" for=" {{ $producto->id }} ">{{$producto->nombre}}</label>
                                                </div>
                                            @endforeach
            
                                        </div> 
                                    @endif
                                    @if ( $contratoCliente->contrato->id != 1 )

                                       <div class="col-6 offset-3 form-group">
                                            <label for="pvp">Precio $:</label>
                                            <input class="form-control" type="text" name="pvp" id="pvp" required value="{{ $contratoCliente->pvp}}">
                                        </div>

                                        @if ($contratoCliente->contrato->id == 3 )
                                            <div class="col-6 offset-3 form-group">
                                                <label for="pvp">Horas de Capacitación:</label>
                                                <input class="form-control" type="text" name="horas_capacitacion" id="horas_capacitacion" required value="{{ $contratoCliente->horas_capacitacion}}">
                                            </div>
                                            <div class="col-6 offset-3 form-group">
                                                    <label for="cuentas_asignadas">Cuentas asignadas:</label>
                                                    <input class="form-control" type="text" name="cuentas_asignadas" id="cuentas_asignadas" required value="{{ $contratoCliente->cuentas_asignadas}}">
                                            </div>
                                        @endif
                                        
                                    @endif

                                    
                                   
        
                                    <input type="hidden" name="productos" id="productos" value="">
                                    <input type="hidden" name="id_contrato" id="id_contrato" value=" {{ $contratoCliente->id}} ">
                                    <input type="hidden" name="tipo_contrato" id="tipo_contrato" value=" {{ $contratoCliente->contrato->id}} ">
        
                                    <div class="form-group col-4 offset-4 mt-5">
                                        <a  href="#" id="check" class="btn btn-large btn-dark px-4 py-2" onclick="enviarFormulario()">Actualizar Contrato</a>
                                    </div>
        
                            </form>
                        </div>
                    </div>
                </div>
            </div>

</div>
@endsection

<script>


        let c = () => Array.from(document.getElementsByTagName("INPUT")).filter(cur => cur.type === 'checkbox' && cur.checked).length > 0;
    
    
        function enviarFormulario(){

            let tipoContrato = document.getElementById('tipo_contrato').value;
           
            
            // si es java premium se valida que haya al menos un producto seleccionado
            if(tipoContrato == 1){
                if(!c()) { // Si NO hay ningun checkbox chequeado.
                    alert("debe marcar algun producto");
                }
                 else {
                    let inputProductos = document.getElementById('productos');
                    let arrayProductos = [];
                    let productos = document.querySelectorAll(".item-producto").forEach(function(element) {
                         if(element.checked){
                             arrayProductos.push(element.value);
                         }
                    });
                    inputProductos.value = arrayProductos.join(',');
    
                    document.getElementById('formContrato').submit();
                }

            }
            else{
                document.getElementById('formContrato').submit();
            }
               
    
        }
    
    
    
    
    
    
    
    
    
    
    </script>