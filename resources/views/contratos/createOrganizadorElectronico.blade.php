@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center m-4">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header justify-content-center">
                        <h3 class="text-center">Generar nuevo contrato</h3>
                </div>
                <div class="card-body">
                    <form  method="POST" action="{{ route('contratos.guardar') }}" id="formContrato">
                            @csrf

                            <div class="col-6 offset-3 form-group">
                                <label for="">Fecha:</label>
                                <input class="form-control" type="date" name="fecha" id="fecha" required>
                            </div>

                            <div class="col-6 offset-3 form-group">
                                <label for="pvp">Precio $:</label>
                                <input class="form-control" type="text" name="pvp" id="pvp" required>
                            </div>

                            <div class="col-6 offset-3 form-group">
                                <label for="horas_capacitacion">Horas de Capacitación:</label>
                                <input class="form-control" type="text" name="horas_capacitacion" id="horas_capacitacion" required>
                            </div>
                            <div class="col-6 offset-3 form-group">
                                    <label for="cuentas_asignadas">Cuentas asignadas:</label>
                                    <input class="form-control" type="text" name="cuentas_asignadas" id="cuentas_asignadas" required>
                            </div>
                            <input type="hidden" name="tipo_contrato" id="tipo_contrato" value=" {{ $tipo_contrato }} ">
                            <input type="hidden" name="cliente" id="tipo_contrato" value=" {{ $cliente }} ">

                            <div class="form-group col-4 offset-4 mt-5">
                                <button type="submit" class="btn btn-large btn-dark px-4 py-2">Generar Contrato</button>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
