@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center m-4">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header justify-content-center">
                        <h3 class="text-center">Cargar contrato Firmado</h3>
                </div>
                <div class="card-body">
                    <form  method="POST" action="{{ route('contratos.guardar-contrato-finalizado') }}" id="formContrato" accept-charset="UTF-8" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Seleccione el archivo</label>
                                <input type="file" class="form-control-file" id="contrato_firmado" name="contrato_firmado">
                              </div>
                           <input type="hidden" name="id_contrato" value="{{$contratoCliente->id}}">
                            <div class="col-6 offset-3 form-group mt-5">
                                <button type="submit" class="btn  btn-dark px-4 py-1">Finalizar contrato</button>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection