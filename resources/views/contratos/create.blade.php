@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center m-4">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header justify-content-center">
                        <h3 class="text-center">Generar nuevo contrato</h3>
                </div>
                <div class="card-body">
                    <form  method="POST" action="{{ route('contratos.seleccionar-tipo-contrato') }}" id="formContrato">
                            @csrf
                            <div class="col-6 offset-3 form-group">
                                <label for="tipo_contrato">Tipo Contrato</label>
                                <select class="form-control" name="tipo_contrato" id="tipo_contrato" onchange="cargarProductos()">
                                    @foreach ($contratos as $contrato)
                                        <option value="{{ $contrato->id }}"> {{ $contrato->tipo }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6 offset-3 form-group">
                                <label for="cliente">Seleccionar cliente</label>
                                <select class="form-control" name="cliente" id="cliente" >
                                        @foreach ($clientes as $cliente)
                                    <option value="{{ $cliente->id }}"> {{ $cliente->nombre_rep_legal }}, {{ $cliente->razon_social}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="col-6 offset-3 form-group">
                                <button type="submit" class="btn  btn-dark px-4 py-1">Continuar</button>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>



</script>
