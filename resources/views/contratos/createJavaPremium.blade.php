@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center m-4">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header justify-content-center">
                        <h3 class="text-center">Generar nuevo contrato</h3>
                </div>
                <div class="card-body">
                    <form  method="POST" action="{{ route('contratos.guardar') }}" id="formContrato">
                            @csrf

                            <div class="col-6 offset-3 form-group">
                                <label for="">Fecha:</label>
                                <input class="form-control" type="date" name="fecha" id="fecha" required>
                            </div>

                            <div class="form-group col-6 offset-3" id="listaProductos">
                                <h5 class="text-center mt-4 mb-4">Seleccione del listado de productos:</h5>

                                @foreach ($productos as $producto)
                                    <div class="form-check">
                                        <input class="form-check-input item-producto" type="checkbox" value="{{ $producto->id }}"   id=" {{ $producto->nombre }} ">
                                        <label class="form-check-label" for=" {{ $producto->id }} ">{{$producto->nombre}}</label>
                                    </div>
                                @endforeach

                            </div>

                            <input type="hidden" name="productos" id="productos" value="">
                            <input type="hidden" name="tipo_contrato" id="tipo_contrato" value=" {{ $tipo_contrato }} ">
                            <input type="hidden" name="cliente" id="tipo_contrato" value=" {{ $cliente }} ">

                            <div class="form-group col-4 offset-4 mt-5">
                                <a  href="#" id="check" class="btn btn-large btn-dark px-4 py-2" onclick="enviarFormulario()">Generar Contrato</a>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>

    // let productos = [];
    // let id_contrato = document.getElementById("tipo_contrato").value;

    // fetch(`/contratos/productos-contrato/${id_contrato}`)
    //     .then(function(response) {
    //          return response.json();
    //      })
    //      .then(function(myJson) {
    //         productos=myJson;
    //         for(i=0;i<productos.length;i++){
    //             $("#listaProductos").append(
    //                                         `<div class="form-check">
    //                                             <input class="form-check-input item-producto" type="checkbox" value=${productos[i].id} id="productos[i].nombre">
    //                                             <label class="form-check-label" for=${productos[i].id}>${productos[i].nombre}</label>
    //                                         </div>`
    //                                         );
    //         }
    //      });

    let c = () => Array.from(document.getElementsByTagName("INPUT")).filter(cur => cur.type === 'checkbox' && cur.checked).length > 0;


    function enviarFormulario(){

            if(!c()) { // Si NO hay ningun checkbox chequeado.
                alert("debe marcar algun producto");
            }
             else {
                let inputProductos = document.getElementById('productos');
                let arrayProductos = [];
                let productos = document.querySelectorAll(".item-producto").forEach(function(element) {
                     if(element.checked){
                         arrayProductos.push(element.value);
                     }
                });
                inputProductos.value = arrayProductos.join(',');

                document.getElementById('formContrato').submit();
            }

    }










</script>
