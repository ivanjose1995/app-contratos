@extends('layouts.app')

@section('content')

<div class="container ">
  
    <div class="row">
        <div class="col-6 offset-3">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <strong>Seguro que desea eliminar el contrato? </strong> se perderá la información.
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-6 offset-3">
                <form action="{{ route ('contratos.eliminar')}}" method="post">
                    @csrf
                        <input type="hidden" name="id_contrato" value="{{$id_contrato}}">
                        <a href=" {{ route('contratos') }} " class="btn btn-primary px-3">Volver</a>
                        <button type="submit" class="btn btn-danger px-4 ml-4">Eliminar</button>
                </form>
        </div>
        
    </div>
    

</div>
    
@endsection