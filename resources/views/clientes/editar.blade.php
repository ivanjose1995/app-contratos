@extends('layouts.app')
@section('content')


    

<div class="container">
    <div class="row justify-content-center m-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header justify-content-center">
                    <h3 class="text-center">Registrar nuevo cliente</h3>
                </div>
                <div class="card-body">
                    <form  method="POST" action="{{ route('clientes.actualizar') }}">
                        @csrf
                        <div class="row">
                            <div class="col-7 form-group">
                                <label for="nombre_rep_legal">Nombre representante legal</label>
                                <input class="form-control" type="text" name="nombre_rep_legal" id="nombre_rep_legal" required value="{{ $cliente->nombre_rep_legal}}">
                            </div>
                            <div class="col-5 form-group">
                                <label for="documento_rep_legal">Documento representante legal</label>
                                <input class="form-control" type="text" name="documento_rep_legal" id="documento_rep_legal" required value="{{ $cliente->documento_rep_legal}}">
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-4  form-group">
                                <label for="razon_social">Razón social</label>
                                <input class="form-control" type="text" name="razon_social" id="razon_social" required value="{{ $cliente->razon_social}}">
                            </div>
                            <div class="col-5 form-group">
                                <label for="nombre_comercial">Nombre comercial</label>
                                <input class="form-control" type="text" name="nombre_comercial" id="nombre_comercial" required value="{{ $cliente->nombre_comercial}}">
                            </div>
                            <div class="col-3  form-group">
                                <label for="ruc">RUC</label>
                                <input class="form-control" type="text" name="ruc" id="ruc" required value="{{ $cliente->ruc}}">
                            </div>  
                        </div>
                        <div class="row">
                            <div class="col-7  form-group">
                                <label for="actividad_comercial">Actividad comercial</label>
                                <input class="form-control" type="text" name="actividad_comercial" id="actividad_comercial" required value="{{ $cliente->actividad_comercial}}">
                            </div>
                            <div class="col-5  form-group">
                                <label for="vendedor">Vendedor</label>
                                <input class="form-control" type="text" name="vendedor" id="vendedor" required value="{{ $cliente->vendedor}}">
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-6 form-group">
                                <label for="email">Correo Electrónico</label>
                                <input class="form-control" type="text" name="email" id="email" required value="{{ $cliente->email}}">
                            </div>
                            <div class="col-6  form-group">
                                <label for="email2">Correo Electrónico 2</label>
                                <input class="form-control" type="text" name="email2" id="email2" required value="{{ $cliente->email2}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6  form-group">
                                <label for="telefono">Telefono</label>
                                <input class="form-control" type="text" name="telefono" id="telefono" required value="{{ $cliente->telefono}}">
                            </div>
                            <div class="col-6  form-group">
                                <label for="telefono2">Telefono 2</label>
                                <input class="form-control" type="text" name="telefono2" id="telefono2" required value="{{ $cliente->telefono2}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4 form-group">
                                <label for="ciudad">Ciudad</label>
                                <input class="form-control" type="text" name="ciudad" id="ciudad" value="{{ $cliente->ciudad}}">
                            </div>
                            <div class="col-8 form-group">
                                <label for="direccion">Dirección</label>
                                 <input class="form-control" type="text" name="direccion" id="direccion" required value="{{ $cliente->direccion}}">
                            </div>
                        </div>    
                        
                        <div class="row">
                            <div class="form-group col-4 ">
                                    <label for="categoria">Seleccione categoria del cliente</label>
                                    <select class="form-control" name="categoria" id="categoria" >
                                            
                                            @foreach ($categorias as $categoria)
                                                <option value="{{ $categoria->id }}"> {{ $categoria->categoria }}</option>
                                            @endforeach
                                        </select>
                            </div>
                           
                        
                        </div>   
                        <input type="hidden" name="id_cliente" value="{{$cliente->id}}">
                        <div class="row mt-4 justify-content-center">
                            <div class=" form-group col-4 ">
                                <button type="submit" class="btn btn-large btn-dark px-4 py-2">ACTUALIZAR CLIENTE</button>
                            </div>
                        </div>                    
                    </form>       
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
