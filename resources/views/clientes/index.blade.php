@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row mb-5 mt-4">
            <div class="col-4 offset-4">
                <h2>Listado de Clientes</h2>
            </div>
            <div class="col-2 offset-2">
                <a  class="btn btn-dark btn-large px-4 py-2" href="{{ route('clientes.crear') }}">Crear Cliente</a>
            </div>
        </div>
        <div class="row">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">Cliente</th>
                        <th scope="col">Ruc</th>
                        <th scope="col">Razón Social</th>
                        <th scope="col">Nombre Comercial</th>
                        <th scope="col">Acciones</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientes as $cliente)
                        <tr>
                            <td>{{ $cliente->nombre_rep_legal }}</td>
                            <td>{{ $cliente->ruc }}</td>
                            <td>{{ $cliente->razon_social }}</td>
                            <td>{{ $cliente->nombre_comercial }}</td>
                            <td>
                                <a class="btn btn-secondary ml-4" href="/clientes/editar/{{$cliente->id}}" >Editar</a>
                                <a class="btn btn-danger ml-4" href="/clientes/confirmar-eliminar/{{$cliente->id}}" >Eliminar</a>
                            </td>
                    
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $clientes->links() }}
        </div>
    </div>
    
@endsection