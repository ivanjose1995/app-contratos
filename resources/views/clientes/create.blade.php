@extends('layouts.app')
@section('content')


    

<div class="container">
    <div class="row justify-content-center m-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header justify-content-center">
                    <h3 class="text-center">Registrar nuevo cliente</h3>
                </div>
                <div class="card-body">
                    <form  method="POST" action="{{ route('clientes.guardar') }}">
                        @csrf
                        <div class="row">
                            <div class="col-7 form-group">
                                <label for="nombre_rep_legal">Nombre representante legal</label>
                                <input class="form-control" type="text" name="nombre_rep_legal" id="nombre_rep_legal" required>
                            </div>
                            <div class="col-5 form-group">
                                <label for="documento_rep_legal">Documento representante legal</label>
                                <input class="form-control" type="text" name="documento_rep_legal" id="documento_rep_legal" required>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-4  form-group">
                                <label for="razon_social">Razón social</label>
                                <input class="form-control" type="text" name="razon_social" id="razon_social" required>
                            </div>
                            <div class="col-5 form-group">
                                <label for="nombre_comercial">Nombre comercial</label>
                                <input class="form-control" type="text" name="nombre_comercial" id="nombre_comercial" required>
                            </div>
                            <div class="col-3  form-group">
                                <label for="ruc">RUC</label>
                                <input class="form-control" type="text" name="ruc" id="ruc" required>
                            </div>  
                        </div>
                        <div class="row">
                            <div class="col-7  form-group">
                                <label for="actividad_comercial">Actividad comercial</label>
                                <input class="form-control" type="text" name="actividad_comercial" id="actividad_comercial" required>
                            </div>
                            <div class="col-5  form-group">
                                <label for="vendedor">Vendedor</label>
                                <input class="form-control" type="text" name="vendedor" id="vendedor" required>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-6 form-group">
                                <label for="email">Correo Electrónico</label>
                                <input class="form-control" type="text" name="email" id="email" required>
                            </div>
                            <div class="col-6  form-group">
                                <label for="email2">Correo Electrónico 2</label>
                                <input class="form-control" type="text" name="email2" id="email2" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6  form-group">
                                <label for="telefono">Telefono</label>
                                <input class="form-control" type="text" name="telefono" id="telefono" required>
                            </div>
                            <div class="col-6  form-group">
                                <label for="telefono2">Telefono 2</label>
                                <input class="form-control" type="text" name="telefono2" id="telefono2" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4 form-group">
                                <label for="ciudad">Ciudad</label>
                                <input class="form-control" type="text" name="ciudad" id="ciudad">
                            </div>
                            <div class="col-8 form-group">
                                <label for="direccion">Dirección</label>
                                 <input class="form-control" type="text" name="direccion" id="direccion" required>
                            </div>
                        </div>    
                        
                        <div class="row">
                            <div class="form-group col-4 ">
                                    <label for="categoria">Seleccione categoria del cliente</label>
                                    <select class="form-control" name="categoria" id="categoria" >
                                            
                                            @foreach ($categorias as $categoria)
                                                <option value="{{ $categoria->id }}"> {{ $categoria->categoria }}</option>
                                            @endforeach
                                        </select>
                            </div>
                           
                        
                        </div>   
                        
                        <div class="row mt-4 justify-content-center">
                            <div class=" form-group col-4 ">
                                <button type="submit" class="btn btn-large btn-dark px-4 py-2">REGISTRAR CLIENTE</button>
                            </div>
                        </div>                    
                    </form>       
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
